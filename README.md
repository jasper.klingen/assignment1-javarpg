This is a project of Jasper Klingen made as the first assignment of the java fullstack develoment course at NoroffUniversity in Oslo.
This project is a backend system of a RPG game. The code is made thru Test Driven Development with Junit. In the test folder one can find 21 different tests used to develop en test the system.
Submissiondate 15-07-2021

Java JDK 16
IntelliJ IDEA 2021 01 03 ultimate edition
JUnit version 5
