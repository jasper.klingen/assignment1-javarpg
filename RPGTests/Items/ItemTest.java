package Items;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;
import Character.*;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    //region WeaponAndArmorEquipTests
    @Test
    void CharacterEquipWeapon_EquipHighLevelWeapon_ThrowInvalidWeaponException() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        Weapon axe = TestItems.createTestWeapon();
        axe.setEquipLevel(2);
        //Act & Assert
        assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(axe));
    }
    @Test
    void CharacterEquipArmor_EquipHighLevelArmor_ThrowInvalidArmorException() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        Armor plate = TestItems.createTestArmor();
        plate.setEquipLevel(2);
        //Act & Assert
        assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(plate));
    }
    @Test
    void CharacterEquipWeapon_EquipWrongWeapon_ThrowInvalidWeaponException() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        Weapon bow = TestItems.createTestBow();
        //Act & Assert
        assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(bow));
    }
    @Test
    void CharacterEquipArmor_EquipWrongArmor_ThrowInvalidArmorException() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        Armor cloth = TestItems.createTestHelmet();
        //Act & Assert
        assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(cloth));
    }
    @Test
    void CharacterEquipWeapon_EquipRightWeapon_ReturnTrue() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        Weapon axe = TestItems.createTestWeapon();
        //Act
        boolean actual = warrior.equipWeapon(axe);
        //Assert
        assertTrue(actual);
    }
    @Test
    void CharacterEquipArmor_EquipRightArmor_ReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        //Arrange
        Warrior warrior = new Warrior();
        Armor plate = TestItems.createTestArmor();
        //Act
        boolean actual = warrior.equipArmor(plate);
        //Assert
        assertTrue(actual);
    }
    //endregion

    //region DPSTests
    @Test
    void DPS_CalculateDPSWithoutWeapon_SameAsExpected() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        //Act
        double expected = 1.00;
        double actual = warrior.getDamagePerSecond();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void DPS_CalculateDPSWithWeapon_SameAsExpected() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        warrior.equipWeapon(TestItems.createTestWeapon());
        //Act
        double expected = (7*1.1)*(1+(5/100));
        double actual = warrior.getDamagePerSecond();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void DPS_CalculateDPSWithWeaponAndArmor_SameAsExpected() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior();
        warrior.equipWeapon(TestItems.createTestWeapon());
        warrior.equipArmor(TestItems.createTestArmor());
        //Act
        double expected = (7*1.1)*(1+((5+1)/100));
        double actual = warrior.getDamagePerSecond();
        //Assert
        assertEquals(expected, actual);
    }
    //endregion
}