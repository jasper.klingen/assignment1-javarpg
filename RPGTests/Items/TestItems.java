package Items;
import Character.PrimaryAttributes;

public class TestItems {
    public static Weapon createTestWeapon(){
        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setEquipLevel(1);
        testWeapon.setEquipSlot(SlotType.WEAPON);
        testWeapon.setWeaponType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        testWeapon.setDamagePerSecond();
        return testWeapon;
    }
    public static Armor createTestArmor(){
        Armor testPlateBody = new Armor();
        testPlateBody.setName("Common Plate Body Armor");
        testPlateBody.setEquipLevel(1);
        testPlateBody.setEquipSlot(SlotType.BODY);
        testPlateBody.setArmorType(ArmorType.PLATE);
        testPlateBody.setArmorPrimaryAttributes(new PrimaryAttributes(2,1,0,0));
        return testPlateBody;
    }
    public static Weapon createTestBow(){
        Weapon testBow = new Weapon();
        testBow.setName("Common Bow");
        testBow.setEquipLevel(1);
        testBow.setEquipSlot(SlotType.WEAPON);
        testBow.setWeaponType(WeaponType.BOW);
        testBow.setDamage(12);
        testBow.setAttackSpeed(0.8);
        testBow.setDamagePerSecond();
        return testBow;
    }
    public static Armor createTestHelmet(){
        Armor testClothHead = new Armor();
        testClothHead.setName("Common Cloth Head Armor");
        testClothHead.setEquipLevel(1);
        testClothHead.setEquipSlot(SlotType.HELMET);
        testClothHead.setArmorType(ArmorType.CLOTH);
        testClothHead.setArmorPrimaryAttributes(new PrimaryAttributes(1,0,0,5));
        return testClothHead;
    }

}
