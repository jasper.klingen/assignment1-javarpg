package Character;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    //region CharacterCreationTest
    @Test
    void Warrior_MakeNewCharacter_CharacterLevelShouldBeOne() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Character character = new Warrior();
        //Act
        int expected = 1;
        int actual = character.getLevel();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void LevelUp_LevelWarriorUpOneLevel_LevelShouldBeTwo() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Character character = new Warrior();
        //Act
        character.levelUp(1);
        int expected = 2;
        int actual = character.getLevel();
        //Assert
        assertEquals(expected, actual);
    }
    @Test
    void LevelUp_LevelWarriorUpZeroLevels_ShouldThrowArgumentException() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Character character = new Warrior();
        //Act & Assert
        assertThrows(IllegalArgumentException.class, () -> character.levelUp(0));
    }
    //endregion

    //region CharacterPrimaryAttributesTest
    @Test
    void Warrior_CreateNewWarrior_PrimaryAttributesShouldBeTenFiveTwoOne() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Character character = new Warrior();
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(10,5,2,1);
        PrimaryAttributes actual = character.getPrimaryAttributes();
        boolean assertion = expected.equals(actual);
        //Assert
        assertTrue(assertion);
    }
    @Test
    void Rogue_CreateNewRogue_PrimaryAttributesShouldBeEightTwoSixOne() throws InvalidWeaponException {
        //Arrange
        Character character = new Rogue();
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(8,2,6,1);
        PrimaryAttributes actual = character.getPrimaryAttributes();
        boolean assertion = expected.equals(actual);
        //Assert
        assertTrue(assertion);
    }
    @Test
    void Ranger_CreateNewRanger_PrimaryAttributesShouldBeEightOneSevenOne() throws InvalidWeaponException {
        //Arrange
        Character character = new Ranger();
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(8,1,7,1);
        PrimaryAttributes actual = character.getPrimaryAttributes();
        boolean assertion = expected.equals(actual);
        //Assert
        assertTrue(assertion);
    }
    @Test
    void Mage_CreateNewMage_PrimaryAttributesShouldBeFiveOneOneEight() throws InvalidWeaponException {
        //Arrange
        Character character = new Mage();
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(5,1,1,8);
        PrimaryAttributes actual = character.getPrimaryAttributes();
        boolean assertion = expected.equals(actual);
        //Assert
        assertTrue(assertion);
    }
    //endregion

    //region CharacterPrimaryAttributesLevelUp1Test
    @Test
    void Warrior_LevelUpOne_PrimaryAttributesShouldBeFifteenEightFourTwo() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Character warrior = new Warrior();
        warrior.levelUp(1);
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(15,8,4,2);
        boolean assertion = expected.equals(warrior.getPrimaryAttributes());
        //Assert
        assertTrue(assertion);
    }
    @Test
    void Rogue_LevelUpOne_PrimaryAttributesShouldBeElevenThreeTenTwo() throws InvalidWeaponException {
        //Arrange
        Character Rogue = new Rogue();
        Rogue.levelUp(1);
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(11,3,10,2);
        boolean assertion = expected.equals(Rogue.getPrimaryAttributes());
        //Assert
        assertTrue(assertion);
    }
    @Test
    void Ranger_LevelUpOne_PrimaryAttributesShouldBeTenTwoTwelveTwo() throws InvalidWeaponException {
        //Arrange
        Character Ranger = new Ranger();
        Ranger.levelUp(1);
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(10,2,12,2);
        boolean assertion = expected.equals(Ranger.getPrimaryAttributes());
        //Assert
        assertTrue(assertion);
    }
    @Test
    void Mage_LevelUpOne_PrimaryAttributesShouldBeEightTwoTwoThirteen() throws InvalidWeaponException {
        //Arrange
        Character Mage = new Mage();
        Mage.levelUp(1);
        //Act
        PrimaryAttributes expected = new PrimaryAttributes(8,2,2,13);
        boolean assertion = expected.equals(Mage.getPrimaryAttributes());
        //Assert
        assertTrue(assertion);
    }
    //endregion

    //region CharacterSecondaryAttributesLevelUp1Test
    @Test
    void Warrior_UpdateSecondaryAttributes_150122() throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Character warrior = new Warrior();
        warrior.levelUp(1);
        //Act
        int[] expected = {150,12,2};
        int[] actual = {
                warrior.getHealth(),
                warrior.getArmourRating(),
                warrior.getElementalResistance()
        };

        //Assert
        assertArrayEquals(expected, actual);
    }
    //endregion

}