package Character;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;

import java.util.ArrayList;

public class Warrior extends Character {

    //In the character constructors the character specific attributes are awarded
    public Warrior() throws InvalidWeaponException, InvalidArmorException {
        this.setLevel(1);
        setPrimaryAttributes(new PrimaryAttributes(10,5,2,1));
        setPrimaryAttributesIncrement(new PrimaryAttributes(5,3,2,1));

        ArrayList<WeaponType> availableWeapons = new ArrayList<WeaponType>();
            availableWeapons.add(WeaponType.AXE);
            availableWeapons.add(WeaponType.HAMMER);
            availableWeapons.add(WeaponType.SWORD);
        setAvailableWeaponTypes(availableWeapons);
        ArrayList<ArmorType> availableArmor= new ArrayList<ArmorType>();
            availableArmor.add(ArmorType.PLATE);
            availableArmor.add(ArmorType.MAIL);
        setAvailableArmorTypes(availableArmor);

        Weapon emptyWeapon = EmptySlots.emptyWeaponSlot();
        emptyWeapon.setWeaponType(WeaponType.AXE);

        this.equipWeapon(emptyWeapon);
        resetDPS();

        Armor emptyArmor = EmptySlots.emptyBodySlot();
        emptyArmor.setArmorType(ArmorType.PLATE);

        this.equipArmor(emptyArmor);
        resetDPS();

    }

    private void resetDPS(){
        this.setTotalPrimaryAttribute(this.getPrimaryAttributes().getStrength() / 100);
        this.setDamagePerSecond();
    }
}