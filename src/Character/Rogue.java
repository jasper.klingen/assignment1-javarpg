package Character;

import Exceptions.InvalidWeaponException;
import Items.ArmorType;
import Items.EmptySlots;
import Items.Weapon;
import Items.WeaponType;

import java.util.ArrayList;

public class Rogue extends Character{
    public Rogue() throws InvalidWeaponException {
        this.setLevel(1);
        setPrimaryAttributes(new PrimaryAttributes(8,2,6,1));
        setPrimaryAttributesIncrement(new PrimaryAttributes(3,1,4,1));

        ArrayList<WeaponType> availableWeapons = new ArrayList<WeaponType>();
            availableWeapons.add(WeaponType.DAGGER);
            availableWeapons.add(WeaponType.SWORD);
        setAvailableWeaponTypes(availableWeapons);

        ArrayList<ArmorType> availableArmor= new ArrayList<ArmorType>();
            availableArmor.add(ArmorType.LEATHER);
            availableArmor.add(ArmorType.MAIL);
        setAvailableArmorTypes(availableArmor);

        Weapon emptyWeapon = EmptySlots.emptyWeaponSlot();
        emptyWeapon.setWeaponType(WeaponType.SWORD);
        this.equipWeapon(emptyWeapon);
    }
}
