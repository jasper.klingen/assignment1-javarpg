package Character;

import Exceptions.InvalidWeaponException;
import Items.ArmorType;
import Items.EmptySlots;
import Items.Weapon;
import Items.WeaponType;

import java.util.ArrayList;

public class Ranger extends Character{
    public Ranger() throws InvalidWeaponException {
        this.setLevel(1);
        setPrimaryAttributes(new PrimaryAttributes(8,1,7,1));
        setPrimaryAttributesIncrement(new PrimaryAttributes(2,1,5,1));

        ArrayList<WeaponType> availableWeapons = new ArrayList<WeaponType>();
            availableWeapons.add(WeaponType.BOW);

        setAvailableWeaponTypes(availableWeapons);
        ArrayList<ArmorType> availableArmor= new ArrayList<ArmorType>();
            availableArmor.add(ArmorType.LEATHER);
            availableArmor.add(ArmorType.MAIL);
        setAvailableArmorTypes(availableArmor);

        Weapon emptyWeapon = EmptySlots.emptyWeaponSlot();
        emptyWeapon.setWeaponType(WeaponType.BOW);
        this.equipWeapon(emptyWeapon);
    }
}
