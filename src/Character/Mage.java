package Character;

import Exceptions.InvalidWeaponException;
import Items.ArmorType;
import Items.EmptySlots;
import Items.Weapon;
import Items.WeaponType;

import java.util.ArrayList;

public class Mage extends Character{
    public Mage() throws InvalidWeaponException {
        this.setLevel(1);
        setPrimaryAttributes(new PrimaryAttributes(5,1,1,8));
        setPrimaryAttributesIncrement(new PrimaryAttributes(3,1,1,5));

        ArrayList<WeaponType> availableWeapons = new ArrayList<WeaponType>();
            availableWeapons.add(WeaponType.STAFF);
            availableWeapons.add(WeaponType.WAND);
        setAvailableWeaponTypes(availableWeapons);

        ArrayList<ArmorType> availableArmor= new ArrayList<ArmorType>();
            availableArmor.add(ArmorType.CLOTH);
        setAvailableArmorTypes(availableArmor);

        Weapon emptyWeapon = EmptySlots.emptyWeaponSlot();
        emptyWeapon.setWeaponType(WeaponType.WAND);
        this.equipWeapon(emptyWeapon);
    }
}
