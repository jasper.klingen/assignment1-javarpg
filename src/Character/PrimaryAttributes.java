package Character;

import java.util.Objects;

public class PrimaryAttributes {
    private int Vitality;
    private int Strength;
    private int Dexterity;
    private int Intelligence;

    public int getVitality() {
        return Vitality;
    }

    public void setVitality(int vitality) {
        Vitality = vitality;
    }

    public int getStrength() {
        return Strength;
    }

    public void setStrength(int strength) {
        Strength = strength;
    }

    public int getDexterity() {
        return Dexterity;
    }

    public void setDexterity(int dexterity) {
        Dexterity = dexterity;
    }

    public int getIntelligence() {
        return Intelligence;
    }

    public void setIntelligence(int intelligence) {
        Intelligence = intelligence;
    }

    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence) {
        Vitality = vitality;
        Strength = strength;
        Dexterity = dexterity;
        Intelligence = intelligence;
    }

    @Override
    public String toString() {
        return  "Vitality: " + this.Vitality +
                "\nStrength: " + this.Strength +
                "\nDexterity: " + this.Dexterity +
                "\nIntelligence: " + this.Intelligence;
    }

    //By overriding the equal methods are easier to test.

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrimaryAttributes that = (PrimaryAttributes) o;
        return Vitality == that.Vitality && Strength == that.Strength && Dexterity == that.Dexterity && Intelligence == that.Intelligence;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Vitality, Strength, Dexterity, Intelligence);
    }

    //By making an add methode it is easier for the character and item attributes to add up in one line
    //
    //All the fields within this object will be able to add up to the corresponding fields of another object and will be put into a new object of the same type
    public PrimaryAttributes add(PrimaryAttributes primaryAttributesIncrement) {
        this.Vitality += primaryAttributesIncrement.Vitality;
        this.Strength += primaryAttributesIncrement.Strength;
        this.Dexterity += primaryAttributesIncrement.Dexterity;
        this.Intelligence += primaryAttributesIncrement.Intelligence;
        return this;
    }
}
