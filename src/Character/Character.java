package Character;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class Character {
    private PrimaryAttributes primaryAttributes;
    private PrimaryAttributes primaryAttributesIncrement;
    private int health;
    private int armourRating;
    private int elementalResistance;
    private int damage;
    private double speed;
    private double damagePerSecond;
    private int level;
    private String name;
    private Weapon weaponEquipped;
    private HashMap<SlotType, Armor> armourEquipped;
    private ArrayList<WeaponType> availableWeaponTypes;
    private ArrayList<ArmorType> availableArmorTypes;
    private double totalPrimaryAttribute;

    //region Getters and Setters
    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }

    public PrimaryAttributes getPrimaryAttributesIncrement() {
        return primaryAttributesIncrement;
    }

    public void setPrimaryAttributesIncrement(PrimaryAttributes primaryAttributesIncrement) {
        this.primaryAttributesIncrement = primaryAttributesIncrement;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getArmourRating() {
        return armourRating;
    }

    public void setArmourRating(int armourRating) {
        this.armourRating = armourRating;
    }

    public int getElementalResistance() {
        return elementalResistance;
    }

    public void setElementalResistance(int elementalResistance) {
        this.elementalResistance = elementalResistance;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDamagePerSecond() {
        return damagePerSecond;
    }

    public void setDamagePerSecond() {
        this.damagePerSecond = this.weaponEquipped.getDamagePerSecond() * ( 1 + (getTotalPrimaryAttribute()*0.01));
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Item getWeaponEquipped() {
        return weaponEquipped;
    }

    public void setWeaponEquipped(Weapon weaponEquipped) {
        this.weaponEquipped = weaponEquipped;
    }

    public HashMap<SlotType, Armor> getArmourEquipped() {
        return armourEquipped;
    }

    public void setArmourEquipped(HashMap<SlotType, Armor> armourEquipped) {
        this.armourEquipped = armourEquipped;
    }

    public ArrayList<WeaponType> getAvailableWeaponTypes() {
        return availableWeaponTypes;
    }

    public void setAvailableWeaponTypes(ArrayList<WeaponType> availableWeaponTypes) {
        this.availableWeaponTypes = availableWeaponTypes;
    }

    public ArrayList<ArmorType> getAvailableArmorTypes() {
        return availableArmorTypes;
    }

    public void setAvailableArmorTypes(ArrayList<ArmorType> availableArmorTypes) {
        this.availableArmorTypes = availableArmorTypes;
    }

    public double getTotalPrimaryAttribute() {
        return totalPrimaryAttribute;
    }

    public void setTotalPrimaryAttribute(double totalPrimaryAttribute) {
        this.totalPrimaryAttribute = totalPrimaryAttribute;
    }
    //endregion

    //Invokes when the character goes a level up
    public void levelUp(int levelsGained){
        if(levelsGained > 0){
            setLevel(getLevel() + levelsGained);
            updateAttributes(levelsGained);
        }
        else{
            throw new IllegalArgumentException();
        }
    }



    //Updates the primary attributes according to the specific character
    private void updateAttributes(int levelsGained){
        for (int i = 0; i < levelsGained; i++) {
            setPrimaryAttributes(
                    getPrimaryAttributes().add(
                            getPrimaryAttributesIncrement()
                    )
            );
            setSecondaryAttributes(getPrimaryAttributes());
            setDamagePerSecond();
        }
    }

    //The secondary Attributes are derived from the primary, thru the following formulas
    private void setSecondaryAttributes(PrimaryAttributes primaryAttributes) {
        this.health = primaryAttributes.getVitality() * 10;
        this.armourRating = primaryAttributes.getStrength() + primaryAttributes.getDexterity();
        this.elementalResistance = primaryAttributes.getIntelligence();
    }

    //When a character equips a weapon this methode is invoked
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {

        //Check if weapon the of the correct type for the character
        if(!this.getAvailableWeaponTypes().contains(weapon.getWeaponType())){
            throw new InvalidWeaponException("this weapon is not available for your class");
        }

        //Check if weapon is right for the level of the character
        if(weapon.getEquipLevel() > this.getLevel()){
            throw new InvalidWeaponException("This weapon in not jet available at your level");
        }
        else{
            this.weaponEquipped = weapon;
            this.setDamagePerSecond();
            return true;
        }
    }
    public boolean equipArmor(Armor armor) throws InvalidArmorException {

        //Check if armor the of the correct type for the character
        if(!this.getAvailableArmorTypes().contains(armor.getArmorType())){
            throw new InvalidArmorException("this armor is not available for your class");
        }
        //Check if armor is right for the level of the character
        if(armor.getEquipLevel() > this.getLevel()){
            throw new InvalidArmorException("This armor is not jet available at your level");
        }

        //Setting the armor to the character in a hashmap to be sure the character only has one helmet body, legs etc
        else{
            HashMap<SlotType, Armor> newArmorSlot = new HashMap<SlotType, Armor>();
            if(this.getArmourEquipped() != null) newArmorSlot = this.getArmourEquipped();
            newArmorSlot.put(armor.getEquipSlot(), armor);
            this.setArmourEquipped(newArmorSlot);
            for (Map.Entry<SlotType, Armor> armorSlot: this.getArmourEquipped().entrySet()) {
                PrimaryAttributes newPA = this.getPrimaryAttributes().add(armorSlot.getValue().getArmorPrimaryAttributes());
            }
            return true;
        }
    }

}
