package Items;

import java.util.Objects;

public abstract class Item {
    private String name;
    private int equipLevel;
    private SlotType equipSlot;

    //region Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEquipLevel() {
        return equipLevel;
    }

    public void setEquipLevel(int equipLevel) {
        this.equipLevel = equipLevel;
    }

    public SlotType getEquipSlot() {
        return equipSlot;
    }

    public void setEquipSlot(SlotType equipSlot) {
        this.equipSlot = equipSlot;
    }
    //endregion

}
