package Items;
import Character.PrimaryAttributes;

//Place holder for the empty slots, to give the attributes a value of zero
public class EmptySlots {
    public static Armor emptyHelmetSlot(){
        Armor noHelmet = new Armor();
        noHelmet.setArmorPrimaryAttributes(new PrimaryAttributes(0,0,0,0));
        return noHelmet;
    }
    public static Armor emptyLegSlot(){
        Armor noLegPiece = new Armor();
        noLegPiece.setArmorPrimaryAttributes(new PrimaryAttributes(0,0,0,0));
        return noLegPiece;
    }
    public static Armor emptyBodySlot(){
        Armor noBodyPiece = new Armor();
        noBodyPiece.setArmorPrimaryAttributes(new PrimaryAttributes(0,0,0,0));
        return noBodyPiece;
    }
    public static Weapon emptyWeaponSlot(){
        Weapon noWeapon = new Weapon();
        noWeapon.setDamage(1);
        noWeapon.setAttackSpeed(1.0);
        return noWeapon;
    }
}
