package Items;

public class Weapon extends Item {
    private int damage;
    private double attackSpeed;
    private double damagePerSecond;
    private WeaponType weaponType;


    //region Getters and Setters
    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
        setDamagePerSecond();
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
        setDamagePerSecond();
    }

    public double getDamagePerSecond() {
        return damagePerSecond;
    }

    public void setDamagePerSecond(double damagePerSecond) {
        this.damagePerSecond = damagePerSecond;
    }

    public void setDamagePerSecond() {
        this.damagePerSecond = this.damage * this.attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }
    //endregion

    public Weapon() {
        setDamagePerSecond();
    }

}
