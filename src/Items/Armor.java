package Items;
import Character.PrimaryAttributes;

public class Armor extends Item{
    private PrimaryAttributes armorPrimaryAttributes;
    private ArmorType armorType;

    //region Getters and Setters
    public PrimaryAttributes getArmorPrimaryAttributes() {
        return armorPrimaryAttributes;
    }

    public void setArmorPrimaryAttributes(PrimaryAttributes armorPrimaryAttributes) {
        this.armorPrimaryAttributes = armorPrimaryAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }
    //endregion
}
