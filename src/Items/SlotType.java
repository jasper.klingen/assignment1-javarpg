package Items;

public enum SlotType {
    HELMET,
    BODY,
    LEGS,
    WEAPON
}
